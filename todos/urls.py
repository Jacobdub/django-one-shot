from django.urls import path
from todos.views import (
    todos_list_list,
    todos_list_detail,
    todo_list_delete,
    todo_item_update,
    todo_list_update,
    todo_list_create,
    todo_item_create,
)

urlpatterns = [
    path("", todos_list_list, name="todos_list_list"),
    path("<int:id>/", todos_list_detail, name="todos_list_detail"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("items/create/", todo_item_create, name="todo_item_create"),
]
